from flask import Flask
from flask_restplus import Api, Resource

flask_app = Flask(__name__)
app = Api(app = flask_app)


#whenever APIs are defined under a namespace, they appear
#under a given heading swagger

# http://127.0.0.1:5000/main
name_space = app.namespace('main', description='Main APIs') #path,description

#define url route
@name_space.route('/')
class MainClass(Resource):
    def get(self):
        return {
            'status': 'got new data'
        }

    def post(self):
        return {
            'status': 'posted new data'
        }

if __name__ == '__main__':
    flask_app.run(debug=True)