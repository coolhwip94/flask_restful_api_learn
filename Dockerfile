FROM python:3.6
WORKDIR /app
ADD flask_api_demo.py requirements.txt /app/
RUN pip install -r requirements.txt
CMD ["python", "flask_api_demo.py"]