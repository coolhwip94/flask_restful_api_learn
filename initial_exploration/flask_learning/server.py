#!\Python_tools\leern-to-code\leern-to-code_ENV\Scripts\python
from flask import Flask, render_template, request
from forms import SignUpForm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'

@app.route('/')
def home():
    return 'Hello World'

@app.route('/about')
def about():
    return 'The about page'

@app.route('/blog')
def blog():
    posts = [{'title': 'Technology in 2019', 'author':'Avi'},
    {'title': 'Expansion of oil in Russia', 'author': 'Bob'}]
    return render_template('blog.html', author='Bob', sunny=False, posts=posts)


#able to define data type ex: int,str,float etc
@app.route('/blog/<string:blog_id>')
def blogpost(blog_id):
    return 'This is the blog post number ' + (blog_id)


#create signup route
@app.route('/signup', methods=['GET', 'POST'])
def signup():
    #call from forms.py SignUpForm class
    form = SignUpForm()
    if form.is_submitted():
        result = request.form
        return render_template('user.html', result=result)
    return render_template('signup.html', form=form)


if __name__=='__main__':
    app.run()