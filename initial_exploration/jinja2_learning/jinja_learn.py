#!\Projects\leern-to-code\leern-to-code_ENV\Scripts\python
from jinja2 import Template

def separator():
    print('\n===============================\n')


#opens jinja template called test.j2
#/Projects/leern-to-code/python_related/jinja2_learning/templates/test.j2 full path if needed
with open('templates/test.j2') as f:
    f_template = Template(f.read())
f.close()

int_1 = {'interface': 'ge-1/0/1', 'vlan': '100','description': 'CUST:FIA:'}
int_2 = {'interface': 'ge-1/0/2', 'vlan': '300','description': 'CUST:EVPL:'}

all_interfaces = [int_1,int_2]


for each_interface in all_interfaces:
    config = f_template.render(
        interface = each_interface['interface'],
        vlan = each_interface['vlan'],
        description = each_interface['description']
    )
    separator()
    print (config)
    separator()
    



