# flask_restful_api_learn

Quick Exploration of creating a flask rest API, utilizing flask-restplus. (fork of flask-restful)

Resources used for learning how to setup flask restful api's.

Dockerfile to build image and run flask.

![image](/uploads/014c645ef0052481f6119449bb6644e3/image.png)



## Kubernetes
Kubernets config `deploy.yml` will deploy N replicas of the example flask app and create a NodePort that maps the container's port `5000` to the port in `deploy.yml`

