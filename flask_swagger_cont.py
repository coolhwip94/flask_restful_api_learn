from flask import Flask, request
from flask_restplus import Api, Resource, fields

flask_app = Flask(__name__)

app = Api(app = flask_app,
                    version = '1.0',
                    title = 'Batch recorder',
                    description = 'Manages names of various batchers'                    
                    )


#whenever APIs are defined under a namespace, they appear
#under a given heading swagger

# http://127.0.0.1:5000/main
name_space = app.namespace('main', description='Main APIs') #path,description
batch_name_space = app.namespace('names',description='Manage Batchers')


# models
# models are used when we want to send
# or receive info in a particular format (json)

model = app.model(
    'Name Model', #specify name of model
    {'name':fields.String( #define param
            required = True,
            description = 'Name of the batcher',
            help =  'name cannot be blank'
            )})

list_of_batchers = {} #creating somewhere to store names (could be a db)

#define APIs

#define url route
@name_space.route('/')
class MainClass(Resource):
    def get(self):
        return {
            'status': 'got new data'
        }

    def post(self):
        return {
            'status': 'posted new data'
        }

#define route and class
@batch_name_space.route('/batch/<int:id>')
class BatchClass(Resource):
    #define documentation
    @app.doc(
        responses={
            200: 'OK',
            400: 'Invalid Argument',
            500: 'Mapping Key Error'},
        params={
            'id': 'Specify the Id associated with the batcher'})

    #GET request
    def get(self, id):
        try:
            name = list_of_batchers[id]
            return {
                'status': 'batcher retrieved',
                'name': list_of_batchers[id]
            }
        except KeyError as e:
            batch_name_space.abort(500, e.__doc__, status = 'Could not retrieve information.', statusCode = '500')
        except Exception as e:
            batch_name_space.abort(400, e.__doc__, status = 'Could not retrieve information', statusCode = '400')
    @app.doc(
        responses={
            200: 'OK',
            400: 'Invalid Argument',
            500: 'Mapping Key Error'},
        params={
            'id': 'Specify the Id associated with the batcher'})

    #defining the method       
    @app.expect(model)

    #POST request
    def post(self,id):
        try:
            list_of_batchers[id] = request.json['name'] # post name to list of bachets
            return {
                'status': 'new batcher added',
                'name': list_of_batchers[id]
            }
        except KeyError as e:
            name_space.abort(500, e.__doc__, status = "Could not save information", statusCode = "500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status = "Could not save information", statusCode = "400")


if __name__ == '__main__':
    flask_app.run(debug=True)